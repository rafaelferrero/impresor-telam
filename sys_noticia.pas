unit sys_noticia;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, PrintersDlgs, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, printers
  {$ifdef windows},windows,shellapi{$endif}{$ifdef linux},LConvEncoding, PrintersDlgs{$endif};

type

  { TForm_Noticia }

  TForm_Noticia = class(TForm)
    B_Imprimir: TButton;
    E_SubtituloNoticia: TEdit;
    E_TituloNoticia: TEdit;
    E_CategoriaNoticia: TEdit;
    E_FechaNoticia: TEdit;
    Image1: TImage;
    L_SubtituloNoticia: TLabel;
    L_TituloNoticia: TLabel;
    L_CategoriaNoticia: TLabel;
    L_FechaNoticia: TLabel;
    M_CuerpoNoticia: TMemo;
    PrintDialog1: TPrintDialog;
    procedure abreFormulario(FormPadre:TForm; PathFile:String; fuente:TFont; cantCharPrinter:integer);
    procedure B_ImprimirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    function getCampo(archivo: TStringList; campo: string; anchoCampo: integer): string;
    procedure impCadenaAnchoFijo(cadena:string; ancho:integer);
    procedure ImprimirCadenaRaw(S:String); //no permite imprimir bajo QT
    procedure ImprimirCodigoEsc(S:String); //no permite imprimir bajo QT
    function AnsiToOemOp(cadena:string):string;
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  Form_Noticia: TForm_Noticia; gNoticia:TStringList; gPathFile:string; gFuente:TFont; gPrinterChar:integer;

implementation

{$R *.lfm}

{ TForm_Noticia }

procedure TForm_Noticia.abreFormulario(FormPadre: TForm; PathFile: String; fuente:TFont; cantCharPrinter:integer);
begin
  gPathFile:=PathFile;
  gFuente:=TFont.Create;
  gFuente:=fuente;
  gPrinterChar:=cantCharPrinter;
  Form_Noticia:=TForm_Noticia.Create(FormPadre);
  Form_Noticia.ShowModal;
end;

procedure TForm_Noticia.B_ImprimirClick(Sender: TObject);
var i:integer;
begin

     printDialog1.title:='Seleccione impresora para imprimir la noticia.';//En linux con el nuevo parche 2013 se ve.
     if PrintDialog1.Execute then begin
          Printer.RawMode:=True;
          Printer.BeginDoc;
          ImprimirCadenaRaw(chr(27)+CHR(64));
          ImprimirCadenaRaw(chr(18)); //cancelar modo condensado
          ImprimirCadenaRaw(CHR(27)+CHR(67)+CHR(48)); //pagina de 48 lineas
          {IMPRIMIR LOS CAMPOS DE LAS NOTICIAS AQUI}
          impCadenaAnchoFijo(trim(E_FechaNoticia.Text)+' - '+trim(E_CategoriaNoticia.Text),gPrinterChar);
          impCadenaAnchoFijo(trim(E_TituloNoticia.Text),gPrinterChar);
          impCadenaAnchoFijo(trim(E_SubtituloNoticia.Text),gPrinterChar);
          for i:=0 to M_CuerpoNoticia.Lines.Count -1 do begin
             impCadenaAnchoFijo(M_CuerpoNoticia.Lines[i],gPrinterChar);
          end;
          ImprimirCadenaRaw('');
          ImprimirCadenaRaw('**************************************');
          ImprimirCadenaRaw('*** develop by www.splatter.com.ar ***');
          {FIN IMPRESION DE NOTICIAS}
          Printer.EndDoc;
          Printer.RawMode:=false;
     end;
end;

procedure TForm_Noticia.ImprimirCadenaRaw(S:String);
const Kenter=CHR(13)+CHR(10);
begin
    {$ifdef windows}
    S:=Utf8ToAnsi(S+Kenter);
    S:=AnsiToOemOp(s);//porque las matriciales esperan cadenas OEM
    {$else}
    S:=UTF8ToCP850(S+Kenter);
    {$endif}

    ImprimirCodigoEsc(S);
end;

procedure TForm_Noticia.ImprimirCodigoEsc(S:String);
var Written: Integer;
begin
    Written:=0;
    Printer.Write(S[1], Length(S), Written);
end;

procedure TForm_Noticia.FormShow(Sender: TObject);
var i,l:integer; componente:tcomponent;
  titulo,subtitulo:string;
begin
  gNoticia:=TStringList.Create;
  gNoticia.Clear;
  gNoticia.LoadFromFile(gPathFile);

  E_CategoriaNoticia.Text:=AnsiToUtf8(getCampo(gNoticia,'Categoria: ',21));
  E_FechaNoticia.Text:=AnsiToUtf8(getCampo(gNoticia,'Fecha    : ',24));

  titulo:=''; i:=6;
  if CompareStr('/',copy(trim(gNoticia.Strings[i]),(Length(trim(gNoticia.Strings[i]))),(Length(trim(gNoticia.Strings[i])))))<>0 then
     titulo:=AnsiToUtf8(copy(trim(gNoticia.Strings[i]),0,(Length(trim(gNoticia.Strings[i])))))
  else titulo:=AnsiToUtf8(Copy(trim(gNoticia.Strings[i]),0,(Length(trim(gNoticia.Strings[i]))-1)));
  repeat
        i:=i+1;
        titulo:=titulo+' '+AnsiToUtf8(trim(gNoticia.Strings[i]));
  until Copy(gNoticia.Strings[i],0,1)=#20;
  E_TituloNoticia.Text:=titulo;

  subtitulo:='';
  if CompareStr(trim(gNoticia.Strings[7]),'')=0 then i:=8
  else i:=9;
  repeat
        subtitulo:=subtitulo+' '+AnsiToUtf8(trim(gNoticia.Strings[i]));
        i:=i+1;
  until Copy(gNoticia.Strings[i],0,1)=#20;

  if CompareStr(subtitulo, UpperCase(subtitulo))=0 then E_SubtituloNoticia.Text:=subtitulo
  else E_SubtituloNoticia.Text:='';


  for l:=i to gNoticia.Count-1 do begin
      if Copy(gNoticia.Strings[l],0,1)=#20 then M_CuerpoNoticia.Append(#9+AnsiToUtf8(Trim(gNoticia.Strings[l])))
      else M_CuerpoNoticia.Append(AnsiToUtf8(Trim(gNoticia.Strings[l])));
  end;

  Form_Noticia.Caption:=Form_Noticia.Caption+E_TituloNoticia.Text;

     for i:=0 to Form_Noticia.ComponentCount-1 do begin
     	componente := Form_Noticia.Components[i];
        if (componente is TButton) then begin
           Tbutton(componente).Font:=gFuente;
           Tbutton(componente).AutoSize:=true;
        end;
        if (componente is TEdit) then TEdit(componente).Font:=gFuente;
        if (componente is TLabel) then TLabel(componente).Font:=gFuente;
        if (componente is TMemo) then TMemo(componente).Font:=gFuente;
     end;

end;

function TForm_Noticia.getCampo(archivo: TStringList; campo: string;
  anchoCampo: integer): string;
var l,i,f:integer; cadena:string;
begin
     Result:='null';
     for l:=0 to archivo.Count-1 do begin
         for i:=0 to Length(archivo.Strings[l]) do begin
  	     f:=i+Length(campo);
             cadena:=Copy(archivo.Strings[l],i,(f-i));
  	     if CompareStr(cadena,campo)=0 then begin
  	        result:=AnsiToUtf8(trim(Copy(archivo.Strings[l],f,anchoCampo)));
  		exit;
  	     end;
  	end;
     end;
end;

procedure TForm_Noticia.impCadenaAnchoFijo(cadena: string; ancho:integer);
var i:integer;
begin
  if Length(cadena)>ancho then begin
     if CompareStr(' ',Copy(cadena,ancho-1,1))<>0 then begin
        for i:=ancho downto 0 do begin
            if CompareStr(' ',Copy(cadena,i-1,1))=0 then begin
               ImprimirCadenaRaw(Trim(Copy(cadena,0,i-1)));
               impCadenaAnchoFijo(Trim(Copy(cadena,i,Length(cadena))),ancho);
               exit;
            end;
        end;
     end
     else begin
          ImprimirCadenaRaw(Trim(Copy(cadena,0,ancho-1)));
          impCadenaAnchoFijo(Trim(Copy(cadena,ancho,Length(cadena))),ancho);
     end;
  end
  else begin
      ImprimirCadenaRaw(cadena);
  end;
end;

//tiene los valores en español
function TForm_Noticia.AnsiToOemOp(cadena:string):string;
var tabla:array[128..255]of char;caracter:char;
    CadenaOem:string; i:integer;
begin
    tabla[199]:=#128;    tabla[252]:=#129;
    tabla[233]:=#130;    tabla[226]:=#131;
    tabla[228]:=#132;    tabla[224]:=#133;
    tabla[229]:=#134;    tabla[231]:=#135;
    tabla[234]:=#136;    tabla[235]:=#137;
    tabla[232]:=#138;    tabla[239]:=#139;
    tabla[238]:=#140;    tabla[236]:=#141;
    tabla[196]:=#142;    tabla[197]:=#143;
    tabla[201]:=#144;    tabla[230]:=#145;
    tabla[198]:=#146;    tabla[244]:=#147;
    tabla[246]:=#148;    tabla[242]:=#149;
    tabla[251]:=#150;    tabla[249]:=#151;
    tabla[255]:=#152;    tabla[214]:=#153;
    tabla[220]:=#154;    tabla[248]:=#155;
    tabla[163]:=#156;    tabla[216]:=#157;
    tabla[215]:=#158;    tabla[131]:=#159;
    tabla[225]:=#160;    tabla[237]:=#161;
    tabla[243]:=#162;    tabla[250]:=#163;
    tabla[241]:=#164;    tabla[209]:=#165;
    tabla[170]:=#166;    tabla[186]:=#167;
    tabla[191]:=#168;    tabla[174]:=#169;
    tabla[172]:=#170;    tabla[189]:=#171;
    tabla[188]:=#160;    tabla[161]:=#161;
    tabla[171]:=#174;    tabla[187]:=#175;
    tabla[166]:=#176;    tabla[166]:=#177;
    tabla[166]:=#178;    tabla[166]:=#179;
    tabla[166]:=#180;    tabla[193]:=#181;
    tabla[194]:=#182;    tabla[192]:=#183;
    tabla[169]:=#184;    tabla[166]:=#185;
    tabla[166]:=#186;
    //tabla[43]:=#187;
    //tabla[43]:=#188;
     tabla[162]:=#189;
    tabla[165]:=#190;   // tabla[43]:=#191;
    //tabla[43]:=#192;     tabla[45]:=#193;
  //  tabla[45]:=#194;     tabla[43]:=#195;
//    tabla[45]:=#196;     tabla[43]:=#197;
    tabla[227]:=#198;    tabla[195]:=#199;
//    tabla[43]:=#200;     tabla[43]:=#201;
  //  tabla[45]:=#202;     tabla[45]:=#203;
    tabla[166]:=#204;   // tabla[45]:=#205;
    //tabla[43]:=#206;
    tabla[164]:=#207;
    tabla[240]:=#208;    tabla[208]:=#209;
    tabla[202]:=#210;    tabla[203]:=#211;
    tabla[200]:=#212;    //tabla[105]:=#213;
    tabla[205]:=#214;    tabla[206]:=#215;
    tabla[207]:=#216;
//    tabla[43]:=#217;    tabla[43]:=#218;
    tabla[166]:=#219;
    //tabla[95]:=#220;
    tabla[166]:=#221;
    tabla[204]:=#222;    tabla[175]:=#223;
    tabla[211]:=#224;    tabla[223]:=#225;
    tabla[212]:=#226;    tabla[210]:=#227;
    tabla[245]:=#228;    tabla[213]:=#229;
    tabla[181]:=#230;    tabla[254]:=#231;
    tabla[222]:=#232;    tabla[218]:=#233;
    tabla[219]:=#234;    tabla[217]:=#235;
    tabla[253]:=#236;    tabla[221]:=#237;
    tabla[175]:=#238;    tabla[180]:=#239;
    tabla[173]:=#240;    tabla[177]:=#241;
    //tabla[61]:=#242;
    tabla[190]:=#243;
    tabla[182]:=#245;    tabla[167]:=#246;
    tabla[247]:=#246;    tabla[184]:=#247;
    tabla[176]:=#248;    tabla[168]:=#249;
    tabla[183]:=#250;    tabla[185]:=#251;
    tabla[179]:=#252;    tabla[178]:=#253;
    tabla[166]:=#254;    tabla[160]:=#255;
//--
    CadenaOem:='';
    for i := 1 to Length(cadena) do begin
        caracter:=cadena[i];
        if ord(caracter)>=128 then begin
            Caracter:=tabla[ord(caracter)];
        end;
        CadenaOem:=CadenaOem+ Caracter;
    end;
    result:=CadenaOem;
end;


end.

