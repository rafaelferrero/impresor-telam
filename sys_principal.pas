unit sys_principal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Grids, Menus, ComCtrls, IniFiles, sys_noticia;

type

  { TForm_Principal }

  TForm_Principal = class(TForm)
    B_EliminarFiltro: TButton;
    B_LitarTodas: TButton;
    B_AplicarFiltro: TButton;
    B_Buscar: TButton;
    B_Recargar: TButton;
    B_ElegirFuente: TButton;
    CB_FiltroFecha: TComboBox;
    CB_FiltroCategoria: TComboBox;
    E_Buscar: TEdit;
    FontDialog1: TFontDialog;
    Image1: TImage;
    L_FiltroFecha: TLabel;
    L_FiltroCategoria: TLabel;
    L_Buscar: TLabel;
    MI_LeerNoticia: TMenuItem;
    PopupMenu1: TPopupMenu;
    SelectDirectoryDialog1: TSelectDirectoryDialog;
    StatusBar1: TStatusBar;
    StringGrid1: TStringGrid;
    Timer1: TTimer;
    Timer2: TTimer;
    procedure B_AplicarFiltroClick(Sender: TObject);
    procedure B_BuscarClick(Sender: TObject);
    procedure B_ElegirFuenteClick(Sender: TObject);
    procedure B_EliminarFiltroClick(Sender: TObject);
    procedure B_LitarTodasClick(Sender: TObject);
    procedure B_RecargarClick(Sender: TObject);
    procedure E_BuscarKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure MI_LeerNoticiaClick(Sender: TObject);
    procedure StringGrid1DblClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure CargarIniFile;
    procedure GenerarIniFile;
    procedure Timer2Timer(Sender: TObject);
    procedure UbicarTelamFiles;
    procedure buscarFuente;
    procedure setearFuente;
    procedure recargarFuente;
    procedure buscarNuevasNoticias;
    procedure initCombos;
    procedure initStringGrid;
    function zerosFiller(i:integer; lenght:integer ):string;
    function isInCombo(combobox: TComboBox; cadena:string):boolean;
    function StyleToStr(fuente:TFont):string;
    function StrToStyle(cadena:String):TFontStyles;
    function getCampo(archivo: TStringList; campo: string; anchoCampo: integer): string;
    function siContiene(archivo: TStringList; frase: string): boolean;

  private
    { private declarations }
  public
    { public declarations }
  end; 

const
     cATodas               =0;
     cAFiltradas           =1;
     cABusquedaSinFiltro   =2;
     cABusquedaFiltrada    =3;

var
  Form_Principal: TForm_Principal;
  gTelam,gValor,gFilesPath:String; gFuente:TFont; gIdStatusText,gAccion,gDiasAntiguedad,gPrintersChars:integer;
  fs:TFormatSettings; gStatusText:TStringList;

implementation

{$R *.lfm}

{ TForm_Principal }

procedure TForm_Principal.B_RecargarClick(Sender: TObject);
var listar:boolean; i,r:integer; F:TSearchRec; Noticia:TStringList;
    titulo,subtitulo,categoria,fechaNoticia:string;
begin
     if gFilesPath='' then CargarIniFile;
     Noticia:=TStringList.create;
     Noticia.Clear;
     r:=1;
     try
     	if FindFirst(gFilesPath+'wrkdir'+PathDelim+'*.*', faAnyFile, F) = 0 then
        repeat
     	      if (F.Attr and faArchive > 0) and (f.Name<>'.') and (f.Name<>'..') then begin
                 Noticia.LoadFromFile(gFilesPath+'wrkdir'+PathDelim+f.Name);
                 fechaNoticia:=Copy(trim(Noticia.Strings[Noticia.Count-2]),0,10);
                 categoria:=AnsiToUtf8(getCampo(Noticia,'Categoria: ',21));

                 //borro las noticias que son mas antiguas que la fecha indicada
                 if (StrToDate(fechaNoticia)<(now-gDiasAntiguedad)) then begin //for Production
                 //if (StrToDate(fechaNoticia)>(now-gDiasAntiguedad)) then begin //for testing only
                    if not DeleteFile(gFilesPath+'wrkdir'+PathDelim+f.Name) then begin
                       ShowMessage('error al borrar el archivo '+f.name);
                    end;
                 end
                 else begin
                      listar:=true;
                      if gAccion<>cATodas then begin //si la accion es todas cargo todo
                         if gAccion=cAFiltradas then begin
                            //si la accion es de filtro unicamente y no cumple con el filtro, no cargo la noticia
                            if CompareStr(CB_FiltroFecha.Text,'TODAS')<>0 then begin
                               if CompareStr(CB_FiltroFecha.Text,fechaNoticia)<>0 then begin
                                  listar:=false;
                               end;
                            end;
                            if CompareStr(CB_FiltroCategoria.Text,'TODAS')<>0 then begin
                               if CompareStr(CB_FiltroCategoria.Text,categoria)<>0 then begin
                                  listar:=false;
                               end;
                            end;
                         end
                         else if gAccion=cABusquedaSinFiltro then begin
                              //si la accion es de busqueda sin filtro y no encuentra el valor, no cargo la noticia
                            if not siContiene(Noticia,E_Buscar.text) then listar:=false;
                         end
                         else if gAccion=cABusquedaFiltrada then begin
                              //si la accion es de busqueda filtrada y no encuentra el valor y no cumple los filtros no cargo la noticia
                            if CompareStr(CB_FiltroFecha.Text,'TODAS')<>0 then begin
                               if CompareStr(CB_FiltroFecha.Text,fechaNoticia)<>0 then begin
                                  listar:=false;
                               end;
                            end;
                            if CompareStr(CB_FiltroCategoria.Text,'TODAS')<>0 then begin
                               if CompareStr(CB_FiltroCategoria.Text,categoria)<>0 then begin
                                  listar:=false;
                               end;
                            end;
                            if not siContiene(Noticia,E_Buscar.text) then listar:=false;
                         end;
                      end;

                      if listar then begin
                         if not isInCombo(CB_FiltroFecha,fechaNoticia) then begin
                            CB_FiltroFecha.Items.Add(fechaNoticia);
                         end;
                         if not isInCombo(CB_FiltroCategoria,categoria) then begin
                            CB_FiltroCategoria.Items.Add(categoria);
                         end;
                         stringGrid1.RowCount:=r+1;
                         StringGrid1.Cells[0,r]:=zerosFiller(StringGrid1.RowCount-1,5);
                         StringGrid1.Cells[1,r]:=Copy(trim(Noticia.Strings[Noticia.Count-2]),0,(Length(trim(Noticia.Strings[Noticia.Count-2]))));
                         StringGrid1.Cells[2,r]:=categoria;

                         titulo:=''; i:=6;
                         if CompareStr('/',copy(trim(Noticia.Strings[i]),(Length(trim(Noticia.Strings[i]))),(Length(trim(Noticia.Strings[i])))))<>0 then
                            titulo:=AnsiToUtf8(copy(trim(Noticia.Strings[i]),0,(Length(trim(Noticia.Strings[i])))))
                         else titulo:=AnsiToUtf8(Copy(trim(Noticia.Strings[i]),0,(Length(trim(Noticia.Strings[i]))-1)));
                         repeat
                               i:=i+1;
                               titulo:=titulo+' '+AnsiToUtf8(trim(Noticia.Strings[i]));
                         until Copy(Noticia.Strings[i],0,1)=#20;
                         stringGrid1.Cells[3,r]:=titulo;

                         subtitulo:='';
                         if CompareStr(trim(Noticia.Strings[7]),'')=0 then i:=8
                         else i:=9;
                         repeat
                               subtitulo:=subtitulo+' '+AnsiToUtf8(trim(Noticia.Strings[i]));
                               i:=i+1;
                         until Copy(Noticia.Strings[i],0,1)=#20;

                         if CompareStr(subtitulo, UpperCase(subtitulo))=0 then StringGrid1.Cells[4,r]:=subtitulo
                         else StringGrid1.Cells[4,r]:='';

                         StringGrid1.Cells[5,r]:=ExtractFileName(f.name);
                         r:=r+1;
                      end;
                 end;
              end;
     	until FindNext(F) <> 0;
     finally
     	FindClose(F);
        StringGrid1.AutoSizeColumns;
     end;
     StringGrid1.SortOrder:=soDescending;
     StringGrid1.SortColRow(true,1);
     CB_FiltroCategoria.DropDownCount:=CB_FiltroCategoria.Items.Count;
     CB_FiltroFecha.DropDownCount:=CB_FiltroFecha.Items.Count;
end;

procedure TForm_Principal.E_BuscarKeyPress(Sender: TObject; var Key: char);
begin
  if Key=#13 then B_BuscarClick(sender);
end;

procedure TForm_Principal.B_ElegirFuenteClick(Sender: TObject);
begin
    buscarFuente;
    setearFuente;
    recargarFuente;
end;

procedure TForm_Principal.B_EliminarFiltroClick(Sender: TObject);
begin
     if (gAccion=cABusquedaSinFiltro) or (gAccion=cATodas) then exit;
     if (gAccion=cABusquedaFiltrada) then begin
        gAccion:=cABusquedaSinFiltro;
     end
     else if gAccion=cAFiltradas then begin
        gAccion:=cATodas;
     end;
     CB_FiltroFecha.Text:='TODAS';
     CB_FiltroCategoria.Text:='TODAS';
     initStringGrid;
     B_RecargarClick(sender);
end;

procedure TForm_Principal.B_LitarTodasClick(Sender: TObject);
begin
    gAccion:=cATodas;
    CB_FiltroFecha.Text:='TODAS';
    CB_FiltroCategoria.Text:='TODAS';
    E_Buscar.Text:='';
    initStringGrid;
    B_RecargarClick(sender);
end;

procedure TForm_Principal.B_AplicarFiltroClick(Sender: TObject);
begin
    if gAccion=cABusquedaSinFiltro then gAccion:=cABusquedaFiltrada
    else if gAccion=cATodas then gAccion:=cAFiltradas;
    initStringGrid;
    B_RecargarClick(sender);
end;

procedure TForm_Principal.B_BuscarClick(Sender: TObject);
begin
    if gAccion=cAFiltradas then begin
       gAccion:=cABusquedaFiltrada;
    end
    else begin
       gAccion:=cABusquedaSinFiltro;
    end;
    initStringGrid;
    B_RecargarClick(sender);
end;

procedure TForm_Principal.FormCreate(Sender: TObject);
var cadena,valor:string; ini:TIniFile;
begin
    gIdStatusText:=0;
    gStatusText:=TStringList.Create;
    gStatusText.Append('Impresor Télam desarrollado con Lazarus/FreePascal por Splatter - soporte@splatter.com.ar');
    gStatusText.Append('--');
    gStatusText.Append('www.splatter.com.ar - Desarrollos de Software multiplataforma a medida con tecnologías Open Source.');
    gStatusText.Append('www.splatter.com.ar - Desarrollo de Sitios Web estáticos y dinámicos a medida y personalizados.');
    gStatusText.Append('www.splatter.com.ar - Asesoramiento para la instalación de redes Wireless, Wired y Fibra Óptica.');
    gStatusText.Append('www.splatter.com.ar - Asesoramiento para la instalación y configuración de Sistemas Servidores.');
    gStatusText.Append('www.splatter.com.ar - info@splatter.com.ar | soporte@splatter.com.ar');

    gAccion:=cATodas;
    gFuente:=TFont.Create;
    fs.LongTimeFormat:='hhnnsszzz';
    fs.ShortDateFormat:='yyyymmdd';
    CargarIniFile;

    cadena:='yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy';
    ini := TIniFile.Create(AppendPathDelim(gTelam)+'HttpXmlClient.ini');
    valor:=trim(ini.ReadString('Parametros', 'ClientKey','')); //este valor hay que tomarlo del ini de telam
    if CompareStr(trim(cadena),trim(valor))<>0 then begin
       ShowMessage('ERROR FATAL!!! La aplicación se cerrará. Llame al soporte técnico'+#13+'(O envíe un mail a soporte@splatter.com.ar).');
       Application.Terminate;
       exit;
     end;

    gFilesPath:=AppendPathDelim(gFilesPath);
    if not DirectoryExists(gFilesPath+'wrkdir') then ForceDirectory(gFilesPath+'wrkdir');

    initStringGrid;
    initCombos;
    buscarNuevasNoticias;
    B_RecargarClick(Sender);
    recargarFuente;
end;

procedure TForm_Principal.MI_LeerNoticiaClick(Sender: TObject);
begin
     Form_Noticia.abreFormulario(Form_Principal,gFilesPath+'wrkdir'+PathDelim+StringGrid1.Cells[5,StringGrid1.Row],gFuente,gPrintersChars);
end;

procedure TForm_Principal.StringGrid1DblClick(Sender: TObject);
begin
     MI_LeerNoticiaClick(Sender);
end;

procedure TForm_Principal.Timer1Timer(Sender: TObject);
begin
     buscarNuevasNoticias;
     B_RecargarClick(sender);
end;

procedure TForm_Principal.CargarIniFile;
var archivo:string; ini:TIniFile;
begin
     archivo:=ExtractFilePath(Application.EXEName)+'settings.ini';
     if not FileExists(archivo) then begin
        GenerarIniFile;
        archivo:=ExtractFilePath(Application.EXEName)+'settings.ini';
     end;
     ini := TIniFile.Create(archivo);
     with ini do begin
          gFilesPath:=AppendPathDelim(trim(ReadString('Configuraciones', 'FilesPath','' )));
          if gFilesPath='' then begin
             UbicarTelamFiles;
             WriteString('Configuraciones', 'FilesPath',gFilesPath);
             gFilesPath:=AppendPathDelim(gFilesPath);
          end;
          gTelam:=AppendPathDelim(trim(ReadString('Configuraciones', 'Telam','' )));
          if gTelam='' then begin
             UbicarTelamFiles;
             WriteString('Configuraciones', 'Telam',gTelam);
             gTelam:=AppendPathDelim(gTelam);
          end;
          gFuente.Name:=trim(ReadString('Configuraciones', 'Nombre','default'));
          gFuente.Size:=StrToInt(trim(ReadString('Configuraciones', 'Tamano','10')));
          gFuente.Color:=StringToColor(trim(ReadString('Configuraciones', 'Color','clDefault')));
          gFuente.Style:=StrToStyle(trim(ReadString('Configuraciones', 'Estilo','[fsBold]')));
          gDiasAntiguedad:=StrToInt(trim(ReadString('Configuraciones', 'DiasAntiguedad','4')));
          gPrintersChars:=StrToInt(trim(ReadString('Configuraciones', 'CantCharPrinter','70')));
          Timer1.Interval:=StrToInt(trim(ReadString('Configuraciones', 'msRefresco','30000')));
     end;
end;

procedure TForm_Principal.GenerarIniFile;
var iniVacio:TStringList; msg:string;
begin
     msg:='ERROR!!!'+#13+'No se encuentra el archivo settings.ini, el mismo será creado.';
     ShowMessage(msg);

     UbicarTelamFiles;

     buscarFuente;

     iniVacio:= TStringList.Create; iniVacio.Clear;
     iniVacio.Add('[Configuraciones]');
     iniVacio.Add('Telam='+gTelam);
     iniVacio.Add('FilesPath='+gFilesPath);
     iniVacio.Add('Nombre='+gFuente.Name);
     iniVacio.Add('Tamano='+IntToStr(gFuente.Size));
     iniVacio.Add('Color='+ColorToString(gFuente.Color));
     iniVacio.Add('Estilo='+StyleToStr(gFuente));
     iniVacio.Add('DiasAntiguedad=4');
     iniVacio.Add('msRefresco=30000');
     iniVacio.Add('CantCharPrinter=70');
     iniVacio.SaveToFile(ExtractFilePath(Application.EXEName)+'settings.ini');

end;

procedure TForm_Principal.Timer2Timer(Sender: TObject);
begin
     if gIdStatusText>gStatusText.Count-1 then gIdStatusText:=0;
     StatusBar1.SimpleText:=gStatusText.Strings[gIdStatusText];
     gIdStatusText:=gIdStatusText+1;
end;

procedure TForm_Principal.UbicarTelamFiles;
var msg:string;
begin
     if gTelam='' then begin
        msg:='Indique dónde se encuentran el archivo HttpXmlClient de Télam';
        ShowMessage(msg);
        if SelectDirectoryDialog1.Execute then begin
           gTelam:=SelectDirectoryDialog1.FileName;
        end;
     end;
     if gFilesPath='' then begin
        msg:='Indique dónde se encuentran las noticias de Télam';
        ShowMessage(msg);
        if SelectDirectoryDialog1.Execute then begin
     	   gFilesPath:=SelectDirectoryDialog1.FileName;
        end;
     end;
end;

procedure TForm_Principal.buscarFuente;
var msg:string;
begin
     msg:='Configure la fuente que desea usar en el programa';
     ShowMessage(msg);
     if FontDialog1.Execute then begin
        gFuente.Assign(FontDialog1.Font);
     end;
end;

procedure TForm_Principal.setearFuente;
var archivo:string; ini:TIniFile;
begin
    archivo:=ExtractFilePath(Application.EXEName)+'settings.ini';
    if not FileExists(archivo) then begin
       GenerarIniFile;
       archivo:=ExtractFilePath(Application.EXEName)+'settings.ini';
    end;
    ini := TIniFile.Create(archivo);
    with ini do begin
        WriteString('Configuraciones', 'Nombre',gFuente.Name );
	WriteString('Configuraciones', 'Tamano',IntToStr(gFuente.Size) );
	WriteString('Configuraciones', 'Color',ColorToString(gFuente.Color) );
	WriteString('Configuraciones', 'Estilo',StyleToStr(gFuente) );
    end;
end;

procedure TForm_Principal.recargarFuente;
var  i:integer; componente:tcomponent;
begin
     with Form_Principal do begin
        for i:=0 to ComponentCount-1 do begin
            componente := Components[i];
            if (componente is TButton) then begin
               Tbutton(componente).Font:=gFuente;
               TButton(componente).AutoSize:=True;
            end;
            if (componente is TStringGrid) then begin
               TStringGrid(componente).Font:=gfuente;
               TStringGrid(componente).AutoSizeColumns;
            end;
            if (componente is TLabel) then TLabel(componente).Font:=gFuente;
        end;
     end;
end;

procedure TForm_Principal.buscarNuevasNoticias;
var F:TSearchRec; Noticia:TStringList; fechaNoticia,nombreNuevo:string;
begin
    if gFilesPath='' then CargarIniFile;
    Noticia:=TStringList.create;
    Noticia.Clear;
     try
     	if FindFirst(gFilesPath + '*.*', faAnyFile, F) = 0 then
        repeat
     	      if (F.Attr and faArchive > 0) and (f.Name<>'.') and (f.Name<>'..') then begin
                 //controlo que la fecha de la noticia no sea mayor que la fecha de la pc, si es así
                 //         resulta evidente que la pc tiene la fecha desactualizada, para evitar problemas
                 //         lo puteo y le cierro el programa
                 Noticia.LoadFromFile(gFilesPath+f.Name);
                 fechaNoticia:=Copy(trim(Noticia.Strings[Noticia.Count-2]),0,10);
                 if (StrToDate(fechaNoticia)>now) then begin
                    ShowMessage('ERROR FATAL!!! - SU SISTEMA SE CERRARÁ'+#13+'Parece que su computadora esta fuera de fecha.'+#13+'Corrija la Fecha de su computadora y vuelva a iniciar el programa.');
                    Application.Terminate;
                    exit;
                 end;
                 //si la fecha de la pc esta bien entonces permito copiar el archivo, ya es menos probable pisar
                 //   una noticia existente
                 nombreNuevo:=DateToStr(now,fs)+'_'+timeToStr(now,fs)+'_'+f.name;
                 if not CopyFile(gFilesPath+f.Name,gFilesPath+'wrkdir'+PathDelim+nombreNuevo,true) then begin
                    ShowMessage('error al copiar el archivo '+nombreNuevo);
                    exit
                 end;
                 //como todo salio bien borro la noticia del directorio de telam.
                 if not DeleteFile(gFilesPath+f.Name) then begin
                    ShowMessage('error al borrar el archivo '+f.name);
                    exit
                 end;
              end;
     	until FindNext(F) <> 0;
     finally
     	FindClose(F);
     end;
end;

procedure TForm_Principal.initCombos;
begin
    CB_FiltroFecha.clear;
    CB_FiltroFecha.Items.Add('TODAS'); CB_FiltroFecha.Text:='TODAS';
    CB_FiltroCategoria.clear;
    CB_FiltroCategoria.Items.Add('TODAS'); CB_FiltroCategoria.Text:='TODAS';
end;

procedure TForm_Principal.initStringGrid;
begin
    StringGrid1.Clear;
    StringGrid1.RowCount:=1;
    StringGrid1.ColCount:=6;
    StringGrid1.Cells[0,0]:='Nro.';
    StringGrid1.Cells[1,0]:='Fecha';
    StringGrid1.Cells[2,0]:='Categoria';
    StringGrid1.Cells[3,0]:='Título';
    StringGrid1.Cells[4,0]:='Subtítulo';
    StringGrid1.Cells[5,0]:='Nombre Archivo';
end;

function TForm_Principal.zerosFiller(i: integer; lenght: integer): string;
var cadena:string; c,zeros:integer;
begin
     cadena:='';
     zeros:=lenght-Length(IntToStr(i));
     if zeros>0 then begin
        for c:=0 to zeros-1 do begin
           cadena:=cadena+'0';
        end;
        cadena:=cadena+IntToStr(i);
     end else cadena:=IntToStr(i);
     Result:=cadena;
end;

function TForm_Principal.isInCombo(combobox: TComboBox; cadena: string
  ): boolean;
var i:integer;
begin
    Result:=false;
    for i:=0 to combobox.Items.Count-1 do begin
       if CompareStr(combobox.Items[i], cadena)=0 then Result:=True;
    end;
end;

function TForm_Principal.StyleToStr(fuente: TFont): string;
var sStyle:string;
begin
     sStyle:='[';
     with fuente do begin
          if( fsBold in Style )then  sStyle := sStyle + 'fsBold, ';
          if( fsItalic in Style )then sStyle := sStyle + 'fsItalic, ';
          if( fsUnderline in Style )then sStyle := sStyle + 'fsUnderline, ';
          if( fsStrikeout in Style )then sStyle := sStyle + 'fsStrikeout, ';
          if sStyle<>'[' then sStyle:=LeftStr(sStyle, Length(sStyle)-2)+']'
          else sStyle:='[]';
     end;
     Result:=sStyle;
end;

function TForm_Principal.StrToStyle(cadena: String): TFontStyles;
var estilo:TFontStyles;
begin
     estilo:=[];
     if( Pos( 'fsBold',cadena ) > 0 ) then estilo := estilo + [ fsBold ];
     if( Pos( 'fsItalic',cadena ) > 0 ) then estilo := estilo + [ fsItalic ];
     if( Pos( 'fsUnderline',cadena ) > 0 ) then estilo := estilo + [ fsUnderline ];
     if( Pos( 'fsStrikeout',cadena ) > 0 ) then estilo := estilo + [ fsStrikeout ];
     Result:=estilo;
end;

function TForm_Principal.getCampo(archivo: TStringList; campo: string;
  anchoCampo: integer): string;
var l,i,f:integer; cadena:string;
begin
     Result:='null';
     for l:=0 to archivo.Count-1 do begin
     	for i:=0 to Length(archivo.Strings[l]) do begin
     		f:=i+Length(campo);
                cadena:=Copy(archivo.Strings[l],i,(f-i));
     		if CompareStr(cadena,campo)=0 then begin
     			result:=trim(Copy(archivo.Strings[l],f,anchoCampo));
     			exit;
     		end;
     	end;
     end;
end;

function TForm_Principal.siContiene(archivo: TStringList; frase: string): boolean;
var l,i,f:integer; cadena:string;
begin
     Result:=false;
     for l:=0 to archivo.Count-1 do begin
     	for i:=0 to Length(archivo.Strings[l]) do begin
     	    f:=i+Length(frase);
            cadena:=Copy(archivo.Strings[l],i,(f-i));
     	    if CompareStr(UpperCase(cadena),UpperCase(frase))=0 then begin
               result:=true;
     	       exit;
     	    end;
     	end;
     end;
end;

end.

