unit sys_instalador;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  IniFiles, process;

type

  { TFrm_Instalador }

  TFrm_Instalador = class(TForm)
    Button1: TButton;
    OpenDialog1: TOpenDialog;
    procedure Button1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Frm_Instalador: TFrm_Instalador;

implementation

{$R *.lfm}

{ TFrm_Instalador }

procedure TFrm_Instalador.Button1Click(Sender: TObject);
var S,findClientKey,archivo,valor:string; ini:TIniFile;  f:TFileStream;
    programa:TProcess;
begin
     findClientKey:='yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy';
     ShowMessage('Busque el archivo HttpXmlClient.ini de Télam');
     if OpenDialog1.Execute then begin
        archivo:=OpenDialog1.FileName;
     end;

     ini := TIniFile.Create(archivo);
     valor:=trim(ini.Readstring('Parametros', 'ClientKey',''));

     // open file and read it into a string
     f:= TFilestream.create( 'ImpresorTelam.exe', fmOpenRead or fmShareDenyNone );
     try
       SetLength( S, f.Size );
       f.read( S[1], Length(S));
     finally
       f.free;
     end;

     // replace first instance of byte sequence only
     S:= Stringreplace( S, findClientKey, valor, []) ;
     // make backup copy of source file
     if not DirectoryExists(AppendPathDelim(ExtractFilePath(Application.ExeName))+'ImpresorTelam') then ForceDirectory(AppendPathDelim(ExtractFilePath(Application.ExeName))+'ImpresorTelam');
     // write modified string
     f:= TFilestream.Create(AppendPathDelim(ExtractFilePath(Application.ExeName))+AppendPathDelim('ImpresorTelam')+'ImpresorTelam.exe',fmCreate);
     try
       f.Write( S[1], Length(S))
     finally
       f.free
     end;

     //comprimir binario.
     try
        programa:=TProcess.Create(nil);
        try
            programa.CommandLine:='upx --best ImpresorTelam/ImpresorTelam.exe';
            programa.Execute;
        except
            on e:exception do ShowMessage('ERROR: '+e.message);
        end;
    finally
        ShowMessage('Instalación Terminada, copie el archivo ImpresorTelam.exe a su ubicación definitiva.');
        programa.Free;
    end;

     Application.Terminate;
     exit;
end;



end.

